# Laravel View
### 185150707111001

### Joey Vilbert

## Screenshoots
![image1](/public/images/About.png)
![image1](/public/images/Contact.png)
![image1](/public/images/Portfolio.png)


## Extend, Section, and For
>Extend and Section

Dabout.blade.php
```php
@extends('about')

@section('content')
    
@stop
```

Dportfolio.blade.php
```php
@extends('portfolio')

@section('content')
    
@stop
```

Dcontact.blade.php
```php
@extends('contact')

@section('content')
    
@stop
```
<br>

>For


portfolio.blade.php
```php
...
 <section id="portfolio" class="light">
      <header class="title">
        <h2>Portfolio</h2>
        @for ($i = 1; $i < 6; $i++)<p>
    No. {{ $i }} Lorem ipsum Cupidatat sit irure nostrud ut deserunt ad enim id laboris. Lorem ipsum Enim proident et eu et minim voluptate eiusmod ullamco commodo aliqua amet.</p><br>
@endfor
      </header>

      <div class="container-fluid">
        <div class="row">
...
```

contact.blade.php
```php
...
  <section id="contact" class="dark">
      <header class="title">
        <h2>Contact <span>Us</span></h2>
        @for ($i = 1; $i < 9; $i++)
    Contact {{ $i }} 0812-1314-3142<br><br>
@endfor
                        <br>
        <p>Lorem ipsum Esse esse cillum nisi cillum deserunt tempor ut pariatur qui officia. Lorem ipsum Magna eu irure sint occaecat cupidatat dolore minim irure cillum.</p>
      </header>
...
```